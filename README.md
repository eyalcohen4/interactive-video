# Interactive Video

## Instructions

```
  $ git clone <repo>
  $ npm install
  $ npm start
  # navigate your browser to http://localhost:1234
```

## Overview

Our approach use an interval to watch the changes in the video timing and to respond accordingliy,
in a reactive manner.

Every 500 milliseconds, the watch is checking the state of the video and our overlays, and decide if we should display a button, track an event, jump to other seconds, or hide something we have already.

The project is written in plain javascript.
