const video = document.querySelector("video");
const spin = document.querySelector(".spin");
const download = document.querySelector(".download");

const FIRST_INTERACTION_TIMEPOINT = 4;
const SECOND_INTERACTION_TIMEPOINT = 21;
const SPIN_WAIT_TIME = 10;

let state = {
  isSpinClicked: false,
  isDownloadVisible: false
};

function setState(newState) {
  state = {
    ...state,
    ...newState
  };
}

function initEventListeners() {
  video.addEventListener("loadeddata", handleVideoLoaded);
  spin.addEventListener("click", handleSpinClick);
  download.addEventListener("click", handleDownloadClick);
}

function handleSpinClick() {
  video.play();
  toggleSpin();
  setState({
    isSpinClicked: true
  });
}

function handleDisplaySpin() {
  video.pause();
  toggleSpin();

  setTimeout(() => {
    // If idle for 10 seconds
    if (!state.isSpinClicked) {
      // jump to second 21
      video.currentTime = SECOND_INTERACTION_TIMEPOINT;
      video.play();
      toggleSpin();
    }
  }, SPIN_WAIT_TIME * 1000);
}

function handleDownloadClick() {
  const os = getOS();

  if (os === "ios") {
    window.open(
      "https://apps.apple.com/us/app/billionaire-casino-slots-777/id1098617974"
    );
    return;
  }

  window.open(
    "https://play.google.com/store/apps/details?id=com.huuuge.casino.texas&hl=en"
  );
}

function handleVideoLoaded() {
  trackStart();

  const interval = setInterval(() => {
    interactionManager();

    if (video.ended) {
      trackEnd();
      clearInterval(interval);
    }
  }, 500);
}

function toggleSpin() {
  spin.classList.toggle("hide");
}

function toggleDownload() {
  setState({
    isDownloadVisible: download.classList.contains("hide")
  });
  download.classList.toggle("hide");
}

function trackStart() {
  fetch("http://www.mocky.io/v2/5be098b232000072006496f5");
}

function trackEnd() {
  fetch("http://www.mocky.io/v2/5be098d03200004d006496f6");
}

function getOS() {
  const userAgent = navigator.userAgent || navigator.vendor || window.opera;

  if (/android/i.test(userAgent)) {
    return "android";
  }

  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "ios";
  }

  return "android";
}

function interactionManager() {
  const shouldDisplaySpin =
    parseInt(video.currentTime) === FIRST_INTERACTION_TIMEPOINT &&
    !state.isSpinClicked &&
    !video.paused;

  const shouldDisplayDownload =
    parseInt(video.currentTime) === SECOND_INTERACTION_TIMEPOINT &&
    !state.isDownloadVisible;

  if (shouldDisplaySpin) {
    handleDisplaySpin();
  }

  if (shouldDisplayDownload) {
    toggleDownload();
  }
}

initEventListeners();
